/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ifpb.dac.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Dijalma Silva <dijalmacz@gmail.com>
 */
@Entity
public class Conserto implements Serializable{
    
    @Id
    @GeneratedValue
    private Long id;
    private double valor;
    @Temporal(TemporalType.DATE)
    private Date dataDoConserto;
    @OneToOne
    private Funcionario funcionario;

    public Conserto() {
    }

    public Conserto(double valor, Date dataDoConserto, Funcionario funcionario) {
        this.valor = valor;
        this.dataDoConserto = dataDoConserto;
        this.funcionario = funcionario;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public Date getDataDoConserto() {
        return dataDoConserto;
    }

    public void setDataDoConserto(Date dataDoConserto) {
        this.dataDoConserto = dataDoConserto;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }
    
}
