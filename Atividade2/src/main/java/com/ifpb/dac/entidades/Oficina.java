/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ifpb.dac.entidades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author Dijalma Silva <dijalmacz@gmail.com>
 */
@Entity
public class Oficina implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    @Embedded
    private Endereco endereco;
    private double saldo;

    @OneToMany
    private List<Funcionario> funcionarios;

    @OneToMany(cascade = CascadeType.REMOVE)
    private List<Conserto> consertos;

    public Oficina() {
    }

    public Oficina(Endereco endereco, double saldo) {
        this.endereco = endereco;
        this.saldo = saldo;
        this.funcionarios = new ArrayList<Funcionario>();
        this.consertos = new ArrayList<Conserto>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public List<Funcionario> getFuncionarios() {
        return funcionarios;
    }

    public void setFuncionarios(List<Funcionario> funcionarios) {
        this.funcionarios = funcionarios;
    }

    public List<Conserto> getConsertos() {
        return consertos;
    }

    public void setConsertos(List<Conserto> consertos) {
        this.consertos = consertos;
    }

    public void addFuncionario(Funcionario funcionario) {
        this.funcionarios.add(funcionario);
    }

    public void removeFuncionario(Funcionario funcionario) {
        this.funcionarios.remove(funcionario);
    }

    public void addConserto(Conserto conserto) {
        this.consertos.add(conserto);
        this.saldo += conserto.getValor();
    }

    public void removeConserto(Conserto conserto) {
        boolean b = this.consertos.remove(conserto);
        System.out.println(b);
        this.saldo -= conserto.getValor();
    }
}
