/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ifpb.dac.main;

import com.ifpb.dac.dao.DaoGeneric;
import com.ifpb.dac.dao.DaoJpa;
import com.ifpb.dac.entidades.Conserto;
import com.ifpb.dac.entidades.Endereco;
import com.ifpb.dac.entidades.Funcao;
import com.ifpb.dac.entidades.Funcionario;
import com.ifpb.dac.entidades.Oficina;
import java.util.Date;

/**
 *
 * @author Dijalma Silva <dijalmacz@gmail.com>
 */
public class App {

    public static void main(String[] args) {

//        DaoJpa<Oficina> daoOficina = new DaoGeneric<Oficina>();
//        DaoJpa<Funcionario> daoFuncionario = new DaoGeneric<Funcionario>();
        DaoJpa<Conserto> daoConserto = new DaoGeneric<Conserto>();

//        
//        Oficina oficina = new Oficina(new Endereco("Tenente Aquino", "Casas Populares", "Cajazeiras"), 0);
//        
//        daoOficina.salvar(oficina);
//        
//        Funcionario funcionario = new Funcionario("Dijalma Silva", "12344", "3763", 2000, Funcao.Gerente);
//        Funcionario funcionario2 = new Funcionario("Jair Anderson", "12233", "33213", 1000, Funcao.Empregado);
//        
//        daoFuncionario.salvar(funcionario);
//        daoFuncionario.salvar(funcionario2);
//        
//        oficina.addFuncionario(funcionario);
//        oficina.addFuncionario(funcionario2);
//        
//        daoOficina.atualizar(oficina);
//        
//        Conserto conserto = new Conserto(300, new Date(), funcionario2);
//        Conserto conserto2 = new Conserto(500, new Date(), funcionario2);
//        Conserto conserto3 = new Conserto(700, new Date(), funcionario);
//        Conserto conserto4 = new Conserto(1500, new Date(), funcionario);
//        
//        daoConserto.salvar(conserto);
//        daoConserto.salvar(conserto2);
//        daoConserto.salvar(conserto3);
//        daoConserto.salvar(conserto4);
//        
//        oficina.addConserto(conserto);
//        oficina.addConserto(conserto2);
//        oficina.addConserto(conserto3);
//        daoOficina.atualizar(oficina);
        Conserto conserto4 = daoConserto.buscarPorId(7L, Conserto.class);
        daoConserto.remover(conserto4);
    }
}
