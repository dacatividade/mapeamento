/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ifpb.dac.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

/**
 *
 * @author Dijalma Silva <dijalmacz@gmail.com>
 * @param <T>
 */
public class DaoGeneric<T> implements DaoJpa<T>{

    
    private final EntityManager em;

    public DaoGeneric() {
        this.em = Persistence.createEntityManagerFactory("atividade2").createEntityManager();
    }

    @Override
    public void salvar(T entity) throws PersistenceException {
        em.getTransaction().begin();
        em.persist(entity);
        em.flush();
        em.getTransaction().commit();
    }

    @Override
    public void atualizar(T entity) throws PersistenceException {
        em.getTransaction().begin();
        em.merge(entity);
        em.flush();
        em.getTransaction().commit();
    }

    @Override
    public void remover(T entity) throws PersistenceException {
        em.getTransaction().begin();
        em.remove(em.merge(entity));
        em.flush();
        em.getTransaction().commit();
    }

    @Override
    public T buscarPorId(Long id, Class entityClass) {
        return (T) em.find(entityClass, id);
    }

    @Override
    public List<T> buscarTodos(Class entityClass) {
        TypedQuery q = em.createQuery("SELECT x FROM " + entityClass.getSimpleName() + " x order by id", entityClass);
        List<T> resultList = q.getResultList();
        return resultList;
    }
    
    
}