/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ifpb.dac.dao;

import java.util.List;
import javax.persistence.PersistenceException;

/**
 *
 * @author Dijalma Silva <dijalmacz@gmail.com>
 * @param <T>
 */
public interface DaoJpa<T> {

    public void salvar(T entity) throws PersistenceException;

    public void atualizar(T entity) throws PersistenceException;

    public void remover(T entity) throws PersistenceException;

    public T buscarPorId(Long id, Class entityClass);

    public List<T> buscarTodos(Class entityClass);

}
