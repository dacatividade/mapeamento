/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ifpb.edu.dac.mapeamento;


import java.util.ArrayList;
import java.util.List;
import javax.persistence.ManyToMany;
import java.io.Serializable;
import javax.persistence.Entity;

/**
 *
 * @author dijalma
 */
@Entity
public class Autor extends Pessoa implements Serializable{

    private String instituicaoVinculada;
    @ManyToMany
    private List<Livro> livros;

    public Autor() {
    }

    public Autor(String instituicaoVinculada) {
        this.instituicaoVinculada = instituicaoVinculada;
        this.livros = new ArrayList<Livro>();
    }

    public String getInstituicaoVinculada() {
        return instituicaoVinculada;
    }

    public void setInstituicaoVinculada(String instituicaoVinculada) {
        this.instituicaoVinculada = instituicaoVinculada;
    }

    public List<Livro> getLivros() {
        return livros;
    }

    public void setLivros(List<Livro> livros) {
        this.livros = livros;
    }

    public void addLivro(Livro l){
        this.livros.add(l);
    }
    
    public void removeLivro(Livro l){
        this.livros.remove(l);
    }
}
