/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ifpb.edu.dac.mapeamento;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Anderson Souza
 */
@Entity
public class Livro implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private String titulo;
    private String ISBN;
    @Temporal(TemporalType.DATE)
    private Date lancamento;
    @ManyToMany(mappedBy = "livros")
    private List<Autor> autores;

    public Livro() {
    }

    public Livro(String titulo, String ISBN, Date lancamento) {
        this.titulo = titulo;
        this.ISBN = ISBN;
        this.lancamento = lancamento;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public Date getLancamento() {
        return lancamento;
    }

    public void setLancamento(Date lancamento) {
        this.lancamento = lancamento;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Autor> getAutores() {
        return autores;
    }

    public void setAutores(List<Autor> autores) {
        this.autores = autores;
    }

    public void addAutor(Autor a){
        this.autores.add(a);
    }
    
    public void removeAutor(Autor a){
        this.autores.remove(a);
    }
}
