/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ifpb.edu.dac.mapeamento;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Anderson Souza
 */
@Entity
public class Aluno extends Pessoa implements Serializable {

    private String matricula;
    @Temporal(TemporalType.DATE)
    private Date dataIngresso;
    private String turma;

    public Aluno() {
    }

    public Aluno(String matricula, Date dataIngresso, String turma) {
        this.matricula = matricula;
        this.dataIngresso = dataIngresso;
        this.turma = turma;
    }

    public Aluno(String matricula, Date dataIngresso, String turma, String nome, String cpf, int idade, Date dataDeNascimento) {
        super(nome, cpf, idade, dataDeNascimento);
        this.matricula = matricula;
        this.dataIngresso = dataIngresso;
        this.turma = turma;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public Date getDataIngresso() {
        return dataIngresso;
    }

    public void setDataIngresso(Date dataIngresso) {
        this.dataIngresso = dataIngresso;
    }

    public String getTurma() {
        return turma;
    }

    public void setTurma(String turma) {
        this.turma = turma;
    }

}
