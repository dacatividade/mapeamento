/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ifpb.edu.dac.main;

import br.ifpb.edu.dac.dao.DaoGeneric;
import br.ifpb.edu.dac.dao.DaoJpa;
import br.ifpb.edu.dac.mapeamento.Pessoa;
import java.util.Date;

/**
 *
 * @author Dijalma Silva <dijalmacz@gmail.com>
 */
public class App {
    
    public static void main(String[] args) {
        
        Pessoa pessoa = new Pessoa("Dijalma", "10001010", 21, new Date());
        DaoJpa<Pessoa> dao = new DaoGeneric<>();
        dao.salvar(pessoa);
    }
}
