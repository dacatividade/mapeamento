/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ifpb.edu.dac.mapeamento;

/**
 *
 * @author Anderson Souza
 */
public enum TelefoneEnum {

    RESIDENCIAL(1), COMERCIAL(2);

    private int tipo;

    private TelefoneEnum(int tipo) {
        this.tipo = tipo;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

}
