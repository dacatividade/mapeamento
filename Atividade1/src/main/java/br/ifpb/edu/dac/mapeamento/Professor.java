/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ifpb.edu.dac.mapeamento;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 *
 * @author Anderson Souza
 */
@Entity
public class Professor extends Pessoa implements Serializable {

    private double salario;
    @OneToMany
    private List<Telefone> telefones;

    public Professor() {
    }

    public Professor(double salario) {
        this.salario = salario;
    }

    public Professor(double salario, String nome, String cpf, int idade, Date dataDeNascimento) {
        super(nome, cpf, idade, dataDeNascimento);
        this.salario = salario;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public List<Telefone> getTelefones() {
        return telefones;
    }

    public void setTelefones(List<Telefone> telefones) {
        this.telefones = telefones;
    }

    public void addTelefone(Telefone t){
        this.telefones.add(t);
    }
    
    public void removeTelefone(Telefone t){
        this.telefones.remove(t);
    }

}
