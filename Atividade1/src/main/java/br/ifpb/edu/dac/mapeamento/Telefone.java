/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ifpb.edu.dac.mapeamento;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author Anderson Souza
 */
@Entity
public class Telefone implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private String numero;
    @Enumerated(EnumType.STRING)
    private TelefoneEnum tipo;

    public Telefone() {
    }

    public Telefone(String numero, TelefoneEnum tipo) {
        this.numero = numero;
        this.tipo = tipo;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public TelefoneEnum getTipo() {
        return tipo;
    }

    public void setTipo(TelefoneEnum tipo) {
        this.tipo = tipo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
