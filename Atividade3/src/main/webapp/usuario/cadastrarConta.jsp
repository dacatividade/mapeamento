<%-- 
    Document   : cadastrarConta
    Created on : Aug 13, 2016, 1:42:29 AM
    Author     : des02
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="../addConta" method="post">
            <label>Codigo</label>
            <input type="text" name="codigo"/>

            <label>Numero</label>
            <input type="text" name="numero" />

            <label>Agência</label>
            <input type="text" name="cpf"/>

            <label>Digito da Agência</label>
            <input type="text" name="digitoAgencia"/>

            <label>Digito da Conta</label>
            <input type="text" name="digitoConta"/>

            <label>Saldo</label>
            <input type="text" name="saldo"/>

            <label>Senha</label>
            <input type="password" name="senha"/>

            <label>Tipo</label>
            <select name="tipo">
                <option value="POUPANCA">Poupança</option>
                <option value="CORRENTE">Corrente</option>
            </select>
            <input type="text" name="email"/>

            <input type="submit" value="Salvar" />

        </form>
    </body>
</html>
