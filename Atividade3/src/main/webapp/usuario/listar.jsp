<%-- 
    Document   : listar
    Created on : Aug 13, 2016, 12:33:44 AM
    Author     : des02
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:forEach items="${sessionScope.contas}" var="conta">
            <form action="../carregaConta?id=${conta.id}" method="post">
                <p>Codigo: ${conta.codigo}</p>
                <p>Agencia: ${conta.agencia}</p>
                <p>Digito da Agencia: ${conta.digitoDaAgencia}</p>
                <p>Digito da Conta: ${conta.digitoDaConta}</p>
                <input type="submit" value="Abrir Conta" />
            </form>
        </c:forEach>
    </body>
</html>
