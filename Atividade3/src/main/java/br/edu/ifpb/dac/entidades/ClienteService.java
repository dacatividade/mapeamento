/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.dac.entidades;

import java.util.List;

/**
 *
 * @author Anderson Souza
 */
public interface ClienteService {

    public Cliente login(Cliente cliente);

    public void salvar(Cliente cliente);

    public List<Conta> buscaContas(Long id);

    public void atualizar(Cliente cliente);

    public Conta buscaConta(Long id);

    public Cliente login(String email, String senha);
}
