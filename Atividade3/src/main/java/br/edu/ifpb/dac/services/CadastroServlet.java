/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.dac.services;

import br.edu.ifpb.dac.entidades.Cliente;
import br.edu.ifpb.dac.entidades.ClienteService;
import br.edu.ifpb.dac.entidades.Endereco;
import br.edu.ifpb.dac.entidades.Telefone;
import java.io.IOException;
import java.sql.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
/**
 *
 * @author Anderson Souza
 */
@WebServlet(urlPatterns = "/add")
public class CadastroServlet extends HttpServlet {
    @EJB
    private ClienteService clienteService;
    
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int codigo = Integer.parseInt(req.getParameter("codigo"));
        String nome = req.getParameter("nome");
        String cpf = req.getParameter("cpf");
        String rg = req.getParameter("rg");
        String rua = req.getParameter("rua");
        String cidade = req.getParameter("cidade");
        String bairro = req.getParameter("bairro");
        String email = req.getParameter("email");
        String senha = req.getParameter("senha");
        Date data = Date.valueOf(req.getParameter("data"));
        String telefone = req.getParameter("telefone");
        Telefone tel = new Telefone(telefone);
        Endereco end = new Endereco(bairro, cidade,rua);
        Cliente cliente = new Cliente(codigo, nome, cpf, rg, end, email, senha, data, tel);
        
        clienteService.salvar(cliente);
        
        resp.sendRedirect("/usuario/home.jsp");
        
        
    }




}
