/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.dac.entidades;

import java.io.Serializable;
import javax.persistence.Embeddable;

/**
 *
 * @author Anderson Souza
 */
@Embeddable
public class Endereco implements Serializable {

    private String bairro;
    private String cidade;
    private String rua;

    public Endereco() {
    }

    public Endereco(String bairro, String cidade, String rua) {
        this.bairro = bairro;
        this.cidade = cidade;
        this.rua = rua;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (this.bairro != null ? this.bairro.hashCode() : 0);
        hash = 37 * hash + (this.cidade != null ? this.cidade.hashCode() : 0);
        hash = 37 * hash + (this.rua != null ? this.rua.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Endereco other = (Endereco) obj;
        if ((this.bairro == null) ? (other.bairro != null) : !this.bairro.equals(other.bairro)) {
            return false;
        }
        if ((this.cidade == null) ? (other.cidade != null) : !this.cidade.equals(other.cidade)) {
            return false;
        }
        if ((this.rua == null) ? (other.rua != null) : !this.rua.equals(other.rua)) {
            return false;
        }
        return true;
    }

}
