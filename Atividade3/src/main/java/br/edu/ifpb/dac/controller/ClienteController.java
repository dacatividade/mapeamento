/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.dac.controller;

import br.edu.ifpb.dac.entidades.Cliente;
import br.edu.ifpb.dac.entidades.ClienteService;
import br.edu.ifpb.dac.entidades.Conta;
import br.edu.ifpb.dac.entidades.Telefone;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.ConversationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *
 * @author Anderson Souza
 */
@Named
@ConversationScoped
public class ClienteController implements Serializable {

    private Cliente cliente;
    private Telefone telefone;
    @EJB
    private ClienteService service;
    private Conta conta;

    @PostConstruct
    public void init() {
        this.cliente = (Cliente) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
        this.telefone = new Telefone();
        this.conta = new Conta();
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setTelefone(Telefone telefone) {
        this.telefone = telefone;
    }

    public Telefone getTelefone() {
        return telefone;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Conta getConta() {
        return conta;
    }

    public void setConta(Conta conta) {
        this.conta = conta;
    }

    public String cadastrarCliente() {
        service.salvar(cliente);
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuario", cliente);
        return "/usuario/index?faces-redirect=true";
    }

    public String editarCliente() {
        service.atualizar(cliente);
        return "/usuario/index?faces-redirect=true";
    }

    public void cadastrarConta() {
        this.cliente.addConta(conta);
        service.atualizar(cliente);
        this.conta = null;
    }
    
    public List<Conta> buscaContas(){
        return service.buscaContas(cliente.getId());
    }
}
