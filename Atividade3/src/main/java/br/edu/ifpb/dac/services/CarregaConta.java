/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.dac.services;

import br.edu.ifpb.dac.entidades.ClienteService;
import br.edu.ifpb.dac.entidades.Conta;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Anderson Souza
 */
@WebServlet(urlPatterns = {"/carregaConta"})
public class CarregaConta extends HttpServlet {

    @EJB
    private ClienteService clienteService;

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = Long.parseLong(req.getParameter("id"));
        Conta conta = clienteService.buscaConta(id);
        
        req.getSession().setAttribute("conta", conta);
        resp.sendRedirect("usuario/conta.jsp");
        
    }
    

}
