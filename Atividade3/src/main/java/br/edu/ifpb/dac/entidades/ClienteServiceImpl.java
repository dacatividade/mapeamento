/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.dac.entidades;

import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Anderson Souza
 */
@Local
@Stateless
public class ClienteServiceImpl implements ClienteService {

    @PersistenceContext
    private EntityManager em;

    public ClienteServiceImpl() {
    }

    public Cliente login(Cliente cliente) {
        try {
            Query query = em.createQuery("SELECT C FROM Cliente C WHERE C.email = :email AND C.senha = :senha", Cliente.class);
            query.setParameter("email", cliente.getEmail());
            query.setParameter("senha", cliente.getSenha());
            return (Cliente) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public void salvar(Cliente cliente) {
        em.persist(cliente);
    }

    public List<Conta> buscaContas(Long id) {
        try {

            Query query = em.createNativeQuery("SELECT * FROM conta LEFT JOIN cliente_conta ON conta.id = cliente_conta.contas_id JOIN cliente on cliente.id = cliente_conta.cliente_id where cliente.id = " + id, Conta.class);
            Query query2 = em.createQuery("SELECT co FROM Conta co JOIN FETCH Cliente cl WHERE Cli");
//            query.setParameter("id", id);
            return query.getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }

    public void atualizar(Cliente cliente) {
        em.merge(cliente);
    }

    public Conta buscaConta(Long id) {
        try {

            Query query = em.createNativeQuery("SELECT * FROM conta LEFT JOIN cliente_conta ON conta.id = cliente_conta.contas_id JOIN cliente on cliente.id = cliente_conta.cliente_id where cliente.id = " + id, Conta.class);
            return (Conta) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public Cliente login(String email, String senha) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
