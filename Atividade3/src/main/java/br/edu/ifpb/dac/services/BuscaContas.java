/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.dac.services;

import br.edu.ifpb.dac.entidades.Cliente;
import br.edu.ifpb.dac.entidades.ClienteService;
import br.edu.ifpb.dac.entidades.Conta;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Anderson Souza
 */
@WebServlet(urlPatterns = {"/lista"})
public class BuscaContas extends HttpServlet {
    
    @EJB
    private ClienteService clienteService;
    
    
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        Cliente cliente = (Cliente) session.getAttribute("usuario");
        List<Conta> listaDeContas = clienteService.buscaContas(cliente.getId());
        req.getSession().setAttribute("contas", listaDeContas);
        resp.sendRedirect("usuario/listar.jsp");
    }

}
