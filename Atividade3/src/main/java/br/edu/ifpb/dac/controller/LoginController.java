/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.dac.controller;

import br.edu.ifpb.dac.entidades.Cliente;
import br.edu.ifpb.dac.entidades.ClienteService;
import java.io.Serializable;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.ConversationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *
 * @author Anderson Souza
 */
@Named
@ConversationScoped
public class LoginController implements Serializable {

    private Cliente cliente;
    @EJB
    private ClienteService clienteService;
    private FacesContext context;

    public LoginController() {
    }

    @PostConstruct
    public void init() {
        this.cliente = new Cliente();
        this.context = FacesContext.getCurrentInstance();
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String logar() {
        Cliente clienteAutenticado = clienteService.login(cliente);
        if (clienteAutenticado != null) {
            Map<String, Object> session = context.getExternalContext().getSessionMap();
            session.put("usuario", clienteAutenticado);
            return "/usuario/index?faces-redirect=true";
        } else {
            return "/index?faces-redirect=true";
        }
    }
}
