/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.dac.services;

import br.edu.ifpb.dac.entidades.Cliente;
import br.edu.ifpb.dac.entidades.ClienteService;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Anderson Souza
 */
@WebServlet(urlPatterns = "/login")
public class LoginServlet extends HttpServlet {

    @EJB
    private ClienteService clienteService;

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("email");
        String senha = req.getParameter("senha");

        Cliente cliente = clienteService.login(email, senha);

        if (cliente == null) {
            resp.sendRedirect("error.html");
        } else {
            HttpSession session = req.getSession();
            session.setAttribute("usuario", cliente);
            resp.sendRedirect("usuario/home.jsp");
        }

//        Endereco end = new Endereco();
//        end.setNum(3);
//        c = new Cliente(0, "nome", "CPF", "rg", end, "bairro", "cidade", "email", new Date());
//        em.getTransaction().begin();
//        em.persist(end);
//        em.getTransaction().commit();
    }

}
