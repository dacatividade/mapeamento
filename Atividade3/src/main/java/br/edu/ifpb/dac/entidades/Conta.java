/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.dac.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author Anderson Souza
 */
@Entity
public class Conta implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private int codigo;
    private String numero;
    private String agencia;
    private String digitoDaAgencia;
    private String digitoDaConta;
    private BigDecimal saldo;
    private String senha;
    @Enumerated(EnumType.STRING)
    private TipoConta tipo;
    @ManyToOne
    private List<Cliente> cliente;

    public Conta() {
        this.cliente = new ArrayList();
    }

    public Conta(int codigo, String numero, String agencia, String digitoDaAgencia, String digitoDaConta, BigDecimal saldo, String senha, TipoConta tipo) {
        this.codigo = codigo;
        this.numero = numero;
        this.agencia = agencia;
        this.digitoDaAgencia = digitoDaAgencia;
        this.digitoDaConta = digitoDaConta;
        this.saldo = saldo;
        this.senha = senha;
        this.tipo = tipo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getDigitoDaAgencia() {
        return digitoDaAgencia;
    }

    public void setDigitoDaAgencia(String digitoDaAgencia) {
        this.digitoDaAgencia = digitoDaAgencia;
    }

    public String getDigitoDaConta() {
        return digitoDaConta;
    }

    public void setDigitoDaConta(String digitoDaConta) {
        this.digitoDaConta = digitoDaConta;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public TipoConta getTipo() {
        return tipo;
    }

    public void setTipo(TipoConta tipo) {
        this.tipo = tipo;
    }

}
