/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.dac.services;

import br.edu.ifpb.dac.entidades.Cliente;
import br.edu.ifpb.dac.entidades.ClienteService;
import br.edu.ifpb.dac.entidades.Conta;
import br.edu.ifpb.dac.entidades.TipoConta;
import java.io.IOException;
import java.math.BigDecimal;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Anderson Souza
 */
@WebServlet(urlPatterns = {"/addConta"})
public class CadastrarConta extends HttpServlet {
    @EJB
    private ClienteService clienteService;
    
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        int codigo = Integer.parseInt(req.getParameter("codigo"));
        String numero = req.getParameter("numero");
        String agencia = req.getParameter("agencia");
        String digitoDaAgencia = req.getParameter("digitoAgencia");
        String digitoDaConta = req.getParameter("digitoConta");
        BigDecimal saldo = new BigDecimal(req.getParameter("saldo"));
        String senha = req.getParameter("senha");
        TipoConta tipo = TipoConta.valueOf(req.getParameter("tipo"));
        Conta conta = new Conta(codigo, numero, agencia, digitoDaAgencia, digitoDaConta, saldo, senha, tipo);
        Cliente cliente = (Cliente) req.getSession().getAttribute("usuario");
        cliente.addConta(conta);
        clienteService.atualizar(cliente);
        
        resp.sendRedirect("usuario/home.jsp");
        
    }

}
