/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.dac.entidades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Anderson Souza
 */
@Entity
public class Cliente implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private int codigo;
    private String nome;
    private String CPF;
    private String rg;
    @Embedded
    private Endereco endereco;
    private String email;
    private String senha;
    @Temporal(TemporalType.DATE)
    private Date dtNasc;
    @OneToMany(cascade = CascadeType.ALL)
    private List<Telefone> telefone;
    @OneToMany(cascade = CascadeType.ALL)
    private List<Conta> contas;

    public Cliente() {
        this.contas = new ArrayList<Conta>();
        this.telefone = new ArrayList<Telefone>();
        this.endereco = new Endereco();
    }

    public Cliente(int codigo, String nome, String CPF, String rg, Endereco endereco, String email, String senha, Date dtNasc, Telefone telefone) {
        this.codigo = codigo;
        this.nome = nome;
        this.CPF = CPF;
        this.rg = rg;
        this.endereco = endereco;
        this.email = email;
        this.senha = senha;
        this.dtNasc = dtNasc;
        this.contas = new ArrayList();
    }

    public void addTelefone(Telefone telefone) {
        this.telefone.add(telefone);
    }

    public void removeTelefone(Telefone telefone) {
        this.telefone.remove(telefone);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCPF() {
        return CPF;
    }

    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Date getDtNasc() {
        return dtNasc;
    }

    public void setDtNasc(Date dtNasc) {
        this.dtNasc = dtNasc;
    }

    public void addConta(Conta conta) {
        this.contas.add(conta);
    }

    public void removeConta(Conta conta) {
        this.contas.remove(conta);
    }

}
